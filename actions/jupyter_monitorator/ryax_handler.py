import json
from pathlib import Path
import paramiko
import time

def handle(input_values: dict) -> None:
    print("Creating ssh key from file...")
    pkey = paramiko.RSAKey.from_private_key_file(input_values.get("ssh_pkey"))

    try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(
            input_values.get("ssh_host"),
            input_values.get("ssh_port"),
            username=input_values.get("ssh_user"),
            pkey=pkey,
        )
        stdin, stdout, stderr = client.exec_command(f"cat {script_file.name}")
        stdout = stdout.readlines() if stdout else []
        stderr = stderr.readlines() if stderr else []
        for line in stdout:
            print("STDOUT===> "+line.strip())
        for line in stderr:
            print("STDERR===> "+line.strip())


        print(f"Deployed use token {input_values.get('jupyter_token')} to connect.")

        cmd = 'squeue -n jupyter -o "%i" -h'
        stdin, stdout, stderr = client.exec_command(cmd)
        stdout = stdout.readlines()
        jobid = -1
        for line in stdout:
            jobid = int(line.strip())
        print(f"Found JOBID '{jobid}'")

        last_log_lines = 0
        logs_file = Path(f"slurm-{jobid}.out")
        print(f"Inspecting {logs_file}")
        time.sleep(2)
        stdin, stdout, stderr = client.exec_command(f"cat slurm-{jobid}.out")
        stdout = stdout.readlines()
        stderr = stderr.readlines()
        for line in stderr:
            print("ERROR Fetching logs...")
            print("STDERR===> " + line.strip())
        current_line = 0
        for line in stdout:
            if current_line > last_log_lines:
                print(line.strip())
                last_log_lines = current_line
            else:
                current_line += 1

    except Exception as e:
        print(
            f"Unexpected exception during bulk upload: {e}"
        )
    finally:
        client.close()



if __name__ == "__main__":
    input_json = {
        "ssh_pkey": "secret",
        "ssh_user": "secret",
        "ssh_host": "secret",
        "ssh_port": "secret",
        "slurm_jobid": 271,
    }
    with open("../secrets.txt") as f:
        secrets = json.load(f)
        for key in secrets:
            if key in input_json:
                input_json[key] = secrets[key]

    handle(input_json)
