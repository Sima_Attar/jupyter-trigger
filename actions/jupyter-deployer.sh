#!/bin/bash
#SBATCH --job-name=jupyter
#SBATCH --time=2-00:00:00

RYAX_PREFIX=/user-api/86cbb39f-5f32-4681-9ea8-1d5d0a84910f/jupyter/

jupyter notebook --ip=0.0.0.0 --NotebookApp.token=2f9ddc38801b9ba4b42fd182637d94e741a50b907b928f84 --NotebookApp.base_url="$RYAX_PREFIX" --NotebookApp.webapp_settings="{'static_url_prefix':\
'"$RYAX_PREFIX"static/'}"
