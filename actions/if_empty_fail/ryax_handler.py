import json

class EmptyException(Exception):
    pass

def handle(input_values: dict) -> None:

    if input_values["input_str"] == "":
        print("Error failed because input_str is empty!")
        raise EmptyException("Empty string as input so I fail...")
    else:
        print(f"String not empty {input_values['input_str']}")

    return


if __name__ == "__main__":
    input_json = {
        "input_str": "",
    }
    with open("../secrets.txt") as f:
        secrets = json.load(f)
        for key in secrets:
            if key in input_json:
                input_json[key] = secrets[key]

    print(json.dumps(handle(input_json), indent=4))
