import json
from pathlib import Path

def handle(input_values: dict) -> None:
    print("Creating ssh key from file...")
    script_file=Path("/tmp/sbatch_script.sh")
    with open(script_file, "w") as sbatch_script:
        sbatch_script.write(f"""#!/bin/bash
#SBATCH --job-name=jupyter
#SBATCH --time=2-00:00:00

RYAX_PREFIX={input_values.get("url_prefix")}

jupyter notebook --ip=0.0.0.0 --NotebookApp.token={input_values.get("jupyter_token")} --NotebookApp.base_url="$RYAX_PREFIX" --NotebookApp.webapp_settings="{{'static_url_prefix':\
'"$RYAX_PREFIX"static/'}}"
        """)

    return { "sbatch_script" : str(script_file) }


if __name__ == "__main__":
    input_json = {
        "url_prefix": "/user-api/86cbb39f-5f32-4681-9ea8-1d5d0a84910f/jupyter/",
        "jupyter_token": "2f9ddc38801b9ba4b42fd182637d94e741a50b907b928f84",
    }
    with open("../secrets.txt") as f:
        secrets = json.load(f)
        for key in secrets:
            if key in input_json:
                input_json[key] = secrets[key]

    print(json.dumps(handle(input_json), indent=4))
