
## jupyter-trigger

Launch a Jupyter Notebook as a Ryax action which is offloaded to an HPC cluster

In the context of a demo for Reims University, who are willing to adopt Ryax if we manage to provide a simple way to deploy this, we want to do a POC to run a Jupyter Notebook on HPC resources through a reconfigurable Ryax workflow.

The Ryax workflow will need to be triggered by a simple HTTP Post or HTTP Json API trigger and then followed by an action with HPC addon to be offloaded to an HPC cluster and run with sbatch. The outputs of the action (and workflow) will need to be the IP to be used so that the user can add on its web browser to make use of the jupyter notebook (from their browser to the HPC cluster).

For this we need to provide an ssh-tunnel to enable the exchange of data from the HPC node to the HPC login node and then to the kubernetes cluster. Initially the ssh-tunnel can be done by the user from a linux terminal but ideally we can try to somehow simplify this if possible by running a workflow which can do the ssh-tunnel. It is important to note that the HPC nodes do not have access to internet. Only ssh protocol can allow the exchange of data with the particular HPC node and this will be allowed, for the name of the user, while the Slurm job will be running.

For this action we can follow and implement the procedure used in this link: https://vikasdhiman.info/ECE490-Neural-Networks/posts/0000-00-06-acg-slurm-jupyter/

and also get inspired by what is proposed in this link: https://researchcomputing.princeton.edu/support/knowledge-base/jupyter

Eventually we can also explore the possibility to launch Jupyter without using singularity container since all the necessary libraries and executables for juptyer, etc are available on the HPC node by using "module load" command.

This is related to issue #392 with the difference that instead of deploying a Ryax action as a service on Kubernetes we offload the execution to an external HPC system with an salloc/sbatch execution mode.




